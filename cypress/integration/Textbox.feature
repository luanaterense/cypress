Feature: Cadastrar usuário
  Como estudante
  Quero realizar testes de componentes
  Para aprender cada dia mais

  Scenario: Testando textbox
    Given que eu acesse a página Automação com Batista
    When preencher os campos de formulário de usuários com o nome "Luana"
    And sobrenome "Terense"
    And e-mail "luana.r.matias@gmail.com"
    And endereço "Rua Um"
    And universidade "Anhanguera Educacional"
    And profissão "QA"
    And gênero "Feminino"
    And idade "29"
    And clicar em CRIAR
    Then deve salvar o cadastro com sucesso conferindo a mensagem