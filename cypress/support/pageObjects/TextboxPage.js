import TextboxElements from '../elements/TextboxElements'
const textboxElements = new TextboxElements
const url = Cypress.config("baseUrl")

class TextboxPage {

    acessarURL(){
        cy.visit(url + "users/new")
    }

    preencherNome(nome){
        cy.get(textboxElements.txtPrimeiroNome()).type(nome)
        
    }

    preencherSobrenome(sobrenome){
        cy.get(textboxElements.txtUltimoNome()).type(sobrenome)
    }

    preencherEmail(email){
        cy.get(textboxElements.txtEmail()).type(email)
    }
  
    preencherEndereco(endereco){
        cy.get(textboxElements.txtEndereco()).type(endereco)
    }

    preencherUniversidade(universidade){
        cy.get(textboxElements.txtUniversidade()).type(universidade)
    }

    preencherProfissao(profissao){
        cy.get(textboxElements.txtProfissao()).type(profissao)
    }
        
    preencherGenero(genero){
        cy.get(textboxElements.txtGenero()).type(genero)
    }
        
    preencherIdade(idade){
        cy.get(textboxElements.txtIdade()).type(idade)
    }

    clicarEmCriar(){
        cy.get(textboxElements.btnCriar()).click();
    }

    metodoDeEspera(){
        cy.wait(50);
    }

    validarInsercao(){
        cy.get(textboxElements.nomeCadastrado()).should('contain', 'Luana')
        cy.get(textboxElements.ultimoNomeCadastrado()).should('contain', 'Terense')
        cy.get(textboxElements.emailCadastrado()).should('contain', 'luana.r.matias@gmail.com')
        cy.get(textboxElements.universidadeCadastrada()).should('contain', 'Anhanguera Educacional')
        cy.get(textboxElements.generoCadastrado()).should('contain', 'Feminino')
        cy.get(textboxElements.profissaoCadastrada()).should('contain', 'QA')
        cy.get(textboxElements.idadeCadastrada()).should('contain', '29')
        cy.get(textboxElements.enderecoCadastrado()).should('contain', 'Rua Um')
    }

    validarMensagemDeRegistroInserido(){
        cy.get(textboxElements.mensagemInsercao()).should('contain', 'Usuário Criado com sucesso')
    }
}

export default TextboxPage;