import DropdownElements from '../elements/DropdownElements'
const dropdownElements = new DropdownElements
const url = Cypress.config("baseUrl")

class DropdownPage {

    acessarURL(){
        cy.visit(url + "buscaelementos/dropdowneselect")
    }
    
}

export default DropdownPage;