/*global Given, Then, When */

import UsuarioPage from '../pageObjects/TextboxPage'
const usuarioPage = new UsuarioPage

Given("que eu acesse a página Automação com Batista", () => {
        usuarioPage.acessarURL();
    }
)

When("preencher os campos de formulário de usuários com o nome {string}", (nome) => {
        usuarioPage.preencherNome(nome);
    }
)


And("sobrenome {string}", (sobrenome) => {
    usuarioPage.preencherSobrenome(sobrenome);
}
)


And("e-mail {string}", (email) => {
    usuarioPage.preencherEmail(email)
}
)

And("endereço {string}", (endereco) => {
    usuarioPage.preencherEndereco(endereco)
}
)

And("universidade {string}", (universidade) => {
    usuarioPage.preencherUniversidade(universidade)
}
)

And("profissão {string}", (profissao) => {
    usuarioPage.preencherProfissao(profissao)
}
)

And("gênero {string}", (genero) => {
    usuarioPage.preencherGenero(genero)
}
)

And("idade {string}", (idade) => {
    usuarioPage.preencherIdade(idade)
}
)

And("clicar em CRIAR", () => {
        usuarioPage.clicarEmCriar();
    }
)

Then("deve salvar o cadastro com sucesso conferindo a mensagem", () => {
        usuarioPage.metodoDeEspera();
        usuarioPage.validarInsercao();
        usuarioPage.validarMensagemDeRegistroInserido();
    }
)