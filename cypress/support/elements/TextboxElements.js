class TextboxElements{
    //Preenchimento dos campos
    txtPrimeiroNome = () => {return '#user_name'}
    txtUltimoNome = () => {return '#user_lastname'}
    txtEmail = () => {return '#user_email'}
    txtEndereco = () => {return '#user_address'}
    txtUniversidade = () => {return '#user_university'}
    txtProfissao = () => {return '#user_profile'}
    txtGenero = () => {return '#user_gender'}
    txtIdade = () => {return '#user_age'}
    btnCriar = () => {return '.green'}

    //Validações
    nomeCadastrado = () => {return '.s9 > :nth-child(3) > .col > :nth-child(1)'}
    ultimoNomeCadastrado = () => {return '.s9 > :nth-child(3) > .col > :nth-child(2)'}
    emailCadastrado = () => {return ':nth-child(3) > .col > :nth-child(3)'}
    universidadeCadastrada  = () => {return ':nth-child(3) > .col > :nth-child(4)'}
    generoCadastrado  = () => {return ':nth-child(3) > .col > :nth-child(5)'}
    profissaoCadastrada = () => {return ':nth-child(3) > .col > :nth-child(6)'}
    idadeCadastrada = () => {return ':nth-child(3) > .col > :nth-child(7)'}
    enderecoCadastrado = () => {return ':nth-child(3) > .col > :nth-child(8)'}

    //Mensagem de inserção efetuada com sucesso
    mensagemInsercao = () => {return '.row.center'}

}

export default TextboxElements;